// Spring, damper and mass system
//
// Author:  Anita Sant'Anna, 2012
//
// Run this example, view Plot, and then 3D
//
// This example models simple system where a mass is connected by a 
// spring and damper to a fixed point. This model is described by 
// the system of ODEs: 
//
//    Fs  = k*(x0-x) - c*x';
//    x'' = (Fs - F)/m;
//      where:
//      Fs: force exerted on mass by spring and damper
//      F: external force applied to the mass (compressing the spring)
//      k: spring constant
//      x0: initial position of the mass
//      x: position of the mass
//      c: damper coefficient
//      m: mass


class SpringDamper(c,k,m,x0)
  private x:=x0; x':=0; x'':=0; F:=0; Fs:=0;
    _3D:=[["Box",[0,0,0],[0.1*m,0.1*m,0.1*m],[0,0,1],[0,0,0]],
         ["Cylinder",[x0-0.5,0,0],[0.01,1],[0,1,0],[0,0,3.1416/2]]];
  end
  Fs = k*(x0-x) - c*x';
  x''=(Fs - F)/m;  
  _3D=[["Box",[x,0,0],[0.1*m,0.1*m,0.1*m],[0,0,1],[0,0,0]],
         ["Cylinder",[0.5+(x-x0)/2-1,0,0],[0.01,-1-(x-x0)],[0,1,0],[0,0,3.1416/2]]];
end


class Main(simulator)
  private 
    // create a spring-damper-mass system with c=1, k=10, m=3, and x0=0
    system := create SpringDamper(1,10,3,0);    
    mode:="push"; t:=0; t':=1; 
    _3D:=["Cylinder",[-1,0,0],[0.01,1],[1,0,0],[3.1416/2,0,0]];  
  end

  _3D=["Cylinder",[-1,0,0],[0.01,1],[1,0,0],[3.1416/2,0,0]];  
  t'=1;

  // a force of F=10N is applied to the system for 0.5 seconds
  switch mode
    case "push"
    system.F=10;
    if t>=0.5
      mode:="free_motion";
    end

    case "free_motion"
    system.F=0;
  end
end

