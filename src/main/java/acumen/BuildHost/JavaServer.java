/*
 * Java server for Raspberry Pi application
 * 
 * Author: 
 * Viktor Frimodig
 * Martin Gustavsson
 * 
 * 2014-11-06: Created
 * 2014-11-07: Improvements
 * 
 */

package acumen.BuildHost;

import java.io.*;
import java.net.*;

public class JavaServer implements Runnable
{
  
  public static String fromRPi;
  public static String toRPi;
  public static ServerSocket server;
  public static Socket client;
  public static BufferedReader in;
  public static PrintWriter out;
  
  public JavaServer() throws IOException
  {
    server = new ServerSocket(8082);
    client = server.accept();
   /* System.out.println("got connection on port 8082"); */
    in = new BufferedReader(new InputStreamReader(client.getInputStream()));
    out = new PrintWriter(client.getOutputStream(),true);
  }
    public JavaServer(int i) throws IOException
    {
        server = new ServerSocket(8082);
        client = server.accept();
   /* System.out.println("got connection on port 8082"); */
        in = new BufferedReader(new InputStreamReader(client.getInputStream()));
        out = new PrintWriter(client.getOutputStream(),true);
    }
 /* public static void JavaServer(int port) throws IOException
  {
    server = new ServerSocket(port);
   // System.out.println("waiting for connection on port " + port);
    client = server.accept();
   // System.out.println("got connection on port " + port);
    in = new BufferedReader(new InputStreamReader(client.getInputStream()));
    out = new PrintWriter(client.getOutputStream(),true); 
  }*/
  
  public static String getSensorData()
  { 
    try 
    {
      return in.readLine();
    } 
    catch (IOException e) 
    {
     // System.out.println("getSensorData: read line failed");
      e.printStackTrace();
      System.exit(-1);
      return "fail"; //???
    }
  }

    @Override
    public void run() {
    // Thread body - sends and receive information.
        while(true) out.println("hej");
    }
  /*
  public static int setServoPosition(int pos)
  {
    if ( pos > 180 || pos < 0 )  return -1;
    
    toRPi = "po>"+pos;
    out.println(toRPi);
    
    return 1;
  }
  
  public static int setServoSpeed(int speed)
  {
    if ( speed > 255 || speed < 0 )  return -1;
    
    toRPi = "sp>" + speed;
    out.println(toRPi);
    return 1;
  }*/
}