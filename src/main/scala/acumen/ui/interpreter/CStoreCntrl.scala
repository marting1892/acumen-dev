package acumen
package ui
package interpreter

import collection.immutable.Queue
import collection.mutable.ListBuffer
import scala.actors._
import InterpreterCntrl._
import acumen.util.Canonical

class CStoreCntrl(val interpreter: CStoreInterpreter) extends InterpreterCntrl {

  def newInterpreterModel = interpreter.newInterpreterModel

  def init(progText: String, consumer:Actor) = new InterpreterActor(progText, consumer) {

    var buffer = Queue.empty[GStore]
    var defaultBufferSize = 200
    var bufferSize = 1 // start off with one step

    override def parse() = 
      if (interpreter.id contains "original") {
        val ast = Parser.run(Parser.prog, progText)
        val dif = SD.run(ast)
        // transform ODEs the old-fashioned way in the original interpreter
        val des = Main.applyPasses(dif, Seq("desugar-local"))
        prog = des
      } else super.parse
    
    def sendChunk {
      val toSend = if (buffer.isEmpty) null else CStoreTraceData(buffer)
      consumer ! Chunk(toSend)
      buffer = Queue.empty[GStore]
    }

    val emergencyActions : PartialFunction[Any,Unit] = {
      case Stop => { sendChunk; exit }
      case Flush => flush
    }

    def flush {
      sendChunk
      react (emergencyActions orElse {
        case GoOn => bufferSize = defaultBufferSize
        case Step => bufferSize = 1
        case msg => println("Unknown msg received by producer: " + msg)
      })
    }

    def produce : Unit = {
      val startTime = System.currentTimeMillis
      val I = interpreter
      val (p, store0) = I.init(prog)
      var store = I.multiStep(p, store0, new StopAtFixedPoint)
      val cstore = I.repr(store)
      val opts = new CStoreOpts
      val playspeed = ui.App.ui.threeDtab.asInstanceOf[ui.threeD.ThreeDTab].playSpeed
      val checkmatchwalltime = ui.App.ui.threeDtab.asInstanceOf[ui.threeD.ThreeDTab].checkMatchWalltime()
      var virtualtime = acumen.util.Canonical.getTime(cstore)
      var simulationTime = 0.0
      var timesteps = 0.0
      var missedDeadline = 0.0
      var percentagemissDL = 0.0
      var slackvalue = 0.0
      var averageSlack = 0.0
      var temptime = 0.0
      var calculationTime = 0.0
      var lastvirtualTime = 0.0
      var updateTime = 0.0
      acumen.util.Canonical.getInSimulator(Name("outputRows",0), cstore) match {
        case VLit(GStr("All"))              => opts.outputRows = OutputRows.All
        case VLit(GStr("WhenChanged"))      => opts.outputRows = OutputRows.WhenChanged
        case VLit(GStr("FinalWhenChanged")) => opts.outputRows = OutputRows.FinalWhenChanged
        case VLit(GStr("ContinuousOnly"))   => opts.outputRows = OutputRows.ContinuousOnly
        case VLit(GStr("Last"))             => opts.outputRows = OutputRows.Last
        case _                              => /* fixme: throw error */
      }
      acumen.util.Canonical.getInSimulator(Name("continuousSkip",0), cstore) match {
        case VLit(GInt(n)) => opts.continuousSkip = n
        case _             => /* fixme: throw error */
      }
      val adder = new FilterDataAdder(opts) {
        var buffer2 = new ListBuffer[(CId,GObject)]
        override def noMoreData() = {
          super.noMoreData()
          if (buffer2.nonEmpty)
            buffer = buffer enqueue buffer2
        }
        def addData(objId: CId, values: GObject) = {
          buffer2 += ((objId, values.toList.filter(mkFilter(values))))
        }
        def continue = {
          if (outputRow) {
            buffer = buffer enqueue buffer2
            buffer2 = new ListBuffer[(CId,GObject)]
          }
          //buffer.size < bufferSize 
          false
        }
      }
      adder.outputRow = true
      cstore.foreach{case (id,v) => adder.addData(id, v)}
      adder.continue
      temptime = System.currentTimeMillis
      loopWhile(!adder.done) {
        reactWithin(0) (emergencyActions orElse {
          case TIMEOUT =>
            temptime = System.currentTimeMillis
            store = I.multiStep(p, store, adder)
            virtualtime = acumen.util.Canonical.getTime(I.repr(store))
            simulationTime = System.currentTimeMillis - startTime
            calculationTime = System.currentTimeMillis - temptime

            // calculate the
            if (calculationTime > (virtualtime - lastvirtualTime) * 1000 / playspeed && virtualtime > lastvirtualTime){
              missedDeadline = missedDeadline + 1
            }
            timesteps = timesteps + 1
            percentagemissDL = missedDeadline / timesteps

            // for synchronizing the simulation with wall clock
            if (checkmatchwalltime){
              // calculate the averageslack
              if ((virtualtime - lastvirtualTime) * 1000 / playspeed < calculationTime){
                //slackvalue = (virtualtime - lastvirtualTime) * 1000 / playspeed - calculationTime + slackvalue
                slackvalue = 0 + slackvalue
              } else {
                slackvalue = (virtualtime - lastvirtualTime) * 1000 / playspeed - calculationTime + slackvalue
              }
              averageSlack = slackvalue / simulationTime
              realtimeTimer(virtualtime, playspeed, simulationTime)
            }else {
              averageSlack = 0
            }
            if ((virtualtime - updateTime) * 1000 > 100){ // update every 100ms
              ui.App.ui.threeDtab.asInstanceOf[ui.threeD.ThreeDTab].averageslack = averageSlack
              ui.App.ui.threeDtab.asInstanceOf[ui.threeD.ThreeDTab].misseddeadline = percentagemissDL
              ui.App.ui.PerMissedDeadline.setText("%.4f".format((percentagemissDL * 100)) + "%")
              ui.App.ui.AverageSlack.setText("%.4f".format((averageSlack * 100)) + "%")
              updateTime = virtualtime
            }
            if (buffer.size >= bufferSize) flush
            lastvirtualTime = virtualtime
        })
      } andThen {
        sendChunk
        consumer ! Done(List("Time to run simulation: %.3fs".format((System.currentTimeMillis - startTime) / 1000.0)))
        //System.err.println("Total simulation time: " + ((System.currentTimeMillis - startTime) / 1000.0) + "s")
      }
    }

    def realtimeTimer(virtualtime: Double, playSpeed: Double, calculatetime: Double) = {
      var sleeptime = 0.0
      var extratime = 0.0
      if (calculatetime > virtualtime * 1000 / playSpeed){
        // do nothing
        sleeptime = 0.0
        extratime = 0.0
      }else {
        sleeptime = (virtualtime * 1000 / playSpeed - calculatetime)
        extratime = (sleeptime - sleeptime.toLong) * 1000000 // To nano sec
        Thread.sleep(sleeptime.toLong, extratime.toInt)
        //print(sleeptime)
        //print("\n")
      }
    }
  }
}
